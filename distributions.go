package random

import(
  "math"
)

const (
MODULE =  2147483647
A  =      16807
LASTXN =  127773
UPTOMOD = -2836
RATIO =  0.46566128e-9            /* 1/MODULE */
)

/*	  
**  Function: rnd32(seed int32) (int32)
**  Remarks : Congruential generator of pseudorandom sequences of numbers
**            uniformly distributed between 1 and 2147483646, using the con-
**            gruential relation: Xn+1 = 16807 * Xn  mod  2147483647 . 
**            The input to the routine is the integer Xn, while the returned 
**            integer is the number Xn+1.
*/
func rnd32(seed int32) (int32){
  var times, rest, prod1, prod2 int32;
 
  times = seed / LASTXN
  rest  = seed - times * LASTXN
  prod1 = times * UPTOMOD
  prod2 = rest * A
  seed  = prod1 + prod2
  if (seed < 0){ 
    seed = seed + MODULE
  }
  return seed
}


/*
**  Function: uniform(a,b float64,  seed *int32) (float64)
**  Return  : a value uniformly distributed between 'a' and 'b'
**  Remarks : The value of '*seed' is changed
*/
func uniform(seed int32,a,b float64) (float64,int32) {
    var u float64
    seed = rnd32 (seed)
    u = float64(seed) * RATIO
    u = a + u * (b-a)
    return u,seed
}

/*	  
**  Function: negexp (mean float64, seed *int32) (float64) 
**  Return  : a value exponentially distributed with mean 'mean'
**  Remarks : The value of '*seed' is changed
*/	  
func negexp (seed int32, mean float64) (float64,int32) {
    var u float64
    seed = rnd32 (seed)
    u = float64(seed) * RATIO
    return ( - mean*math.Log(u) ),seed
}

/*
** Function: pareto(a, min, float64, seed *int32) (float64)
** Returns : a value distributed with Pareto pdf
** Remarks : the value of seed is changed
*/

//TODO: check reverse !
/*func pareto(... float64, seed *int32) (float64) {
  var u float64
  *seed = rnd32(*seed)
  u = float64(*seed)*RATIO
  return (min/math.Pow(u,1/a))
}*/

/*
** Function: zipf(s float64, seed *int32) (float64)
** Returns : a value distributed with Zipf pdf
** Remarks : the value of seed is changed
** Algorithm from page 551 of:
** Devroye, Luc (1986) `Non-uniform random variate generation',
** Springer-Verlag: Berlin.   ISBN 3-540-96305-7 (also 0-387-96305-7)
*/
func zipf(seed int32,a float64)(float64,int32){
    var b,u,v,t,x float64
    for {
      b = math.Pow(2,a-1)
      seed=rnd32(seed)
      u = float64(seed)*RATIO
      seed=rnd32(seed)
      v = float64(seed)*RATIO

      x=math.Floor(math.Pow(u,-1/(a-1)))
      t=math.Pow(1+1/x,a-1)
      if v*x*(t-1)/(b-1)<=t/b{
        return x,seed
      }
    }
}
