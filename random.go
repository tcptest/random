/* This package is a port of the random.h file given to us by Prof. Leonardi at
** Polytecnico di Torino
*/
package random
import ("errors"
    "fmt"
)


const (  // iota is reset to 0
        UNIFORM = iota
        NEGEXP = iota  // c0 == 0
        ZIPF = iota  // c1 == 1
)


func GetValue(seed int32, distribution int, arguments ...float64) (float64,int32,error){
  switch{
    case distribution == UNIFORM && len(arguments)==2:
      value,seed:=uniform(seed,arguments[0],arguments[1])
      return value,seed,nil
    case distribution == NEGEXP && len(arguments)==1:
      value,seed:=negexp(seed,arguments[0])
      return value,seed,nil
    case distribution == ZIPF && len(arguments)==1:
      value,seed:=zipf(seed,arguments[0])
      return value,seed,nil
  }
  return 0,seed,errors.New("Invalid distribution and/or number of arguments")
}
